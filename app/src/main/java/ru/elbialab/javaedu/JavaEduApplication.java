package ru.elbialab.javaedu;

import android.app.Application;

import ru.elbialab.education.EducationActivity;

public class JavaEduApplication extends Application {
   @Override
   public void onCreate() {
      super.onCreate();
      EducationActivity.solver = new Solver();
   }
}
