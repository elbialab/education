package ru.elbialab.javaedu;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.view.MotionEvent;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

import ru.elbialab.education.SolveDrawer;
import ru.elbialab.education.ViewInfo;

import static java.lang.Math.PI;
import static java.lang.Math.log;
import static java.lang.Math.sqrt;

public class Solver extends SolveDrawer {

   static {
      taskNumber = 33;
      showPreview = true;
   }

   class Point {
      float x;
      float y;

      Point(float x, float y) {
         this.x = x;
         this.y = y;
      }

   }

   List<Point> p = new ArrayList<>();
   Random rnd = new Random();
   Point a = new Point (30, 40);
   Point b = new Point (80, 150);
   Point c = new Point (150, 60);
   Point ps[] = {a, b, c};
   Point r = new Point (rnd.nextInt(200), rnd.nextInt(200));

   @Override
   public void onUpdate(float dt, ViewInfo viewInfo) {
      Point c = ps[rnd.nextInt(ps.length)];
      p.add(r);
      r = new Point((c.x + r.x)/2, (c.y + r.y)/2);
   }

   @Override
   public void onDraw(Canvas canvas, Paint paint, ViewInfo viewInfo) {
      paint.setColor(Color.BLUE);
      for (Point k:p) {
         canvas.drawPoint(k.x, k.y, paint);
      }
      paint.setColor(Color.GREEN);
      for (Point k : ps) {
         canvas.drawCircle(k.x, k.y, 1, paint);
      }
      paint.setColor(Color.RED);
      canvas.drawPoint(r.x, r.y, paint);
   }
}







